# ChangeLog

## 1.0.2 (2024-01-12)

**Changed:**
* Minor lib dependency updates

## 1.0.1 (2023-07-17)

**Changed:**
* Code refactoring to improve future handling 

## 1.0.0 (2022-10-31)

Initial release
