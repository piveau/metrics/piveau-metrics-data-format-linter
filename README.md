# Metrics Data Format Linter

## Table of Contents
1. [Build](#build)
1. [Run](#run)
1. [Docker](#docker)
1. [Configuration](#configuration)
    1. [Environment](#environment)
    1. [Logging](#logging)
1. [License](#license)

## Build

Requirements:
* Git
* Maven 3
* Java 17

```bash
$ git clone <gitrepouri>
$ cd piveau-metrics-data-format-linter
$ mvn package
```

## Run

```bash
$ java -jar target/data-format-linter.jar
```

## Docker

Build docker image:
```bash
$ docker build -t piveau/piveau-metrics-data-format-linter .
```

Run docker image:
```bash
$ docker run -it -p 8080:8080 piveau/piveau-metrics-data-format-linter
```

## Configuration

This pipe module takes the `address` from the pipe object to download the file.

### Environment

| Variable             | Description                                    | Default Value |
|:---------------------|:-----------------------------------------------|:--------------|
| `PIVEAU_PLACEHOLDER` | Placeholder for the first environment variable | `default`     |

### Logging
See [logback](https://logback.qos.ch/documentation.html) documentation for more details

| Variable               | Description                                       | Default Value |
|:-----------------------|:--------------------------------------------------|:--------------|
| `PIVEAU_LOGSTASH_HOST` | The host of the logstash service                  | `logstash`    |
| `PIVEAU_LOGSTASH_PORT` | The port the logstash service is running          | `5044`        |
| `PIVEAU_LOG_LEVEL`     | The general log level for the `io.piveau` package | `INFO`        |

## License

[Apache License, Version 2.0](LICENSE.md)
