package io.piveau.metrics


import io.piveau.dcatap.FormatVisitor
import io.piveau.dcatap.MediaTypeVisitor
import io.vertx.core.json.JsonObject
import io.vertx.kotlin.core.json.jsonObjectOf
import org.apache.jena.rdf.model.Model
import org.apache.jena.rdf.model.Statement
import org.apache.jena.vocabulary.DCAT
import org.apache.jena.vocabulary.DCTerms
import org.slf4j.LoggerFactory

internal val log = LoggerFactory.getLogger("io.piveau.metrics.DatasetHelper")

data class Distribution(
    val accessURLs: List<StatementUris>,
    val downloadURLs: List<StatementUris>,
    val formatIsCSV: Boolean
)

data class StatementUris(val distributionUri: String, val property: String, val uri: String)

fun Model.datasetDistributions(): List<Distribution> {

    return listObjectsOfProperty(DCAT.distribution)
        .filterKeep { it.isResource }
        .mapWith { it.asResource() }
        .mapWith { distribution ->
            val format = distribution.listProperties(DCTerms.format)
                .mapWith { it.`object`.visitWith(FormatVisitor) as JsonObject }


            val mediaType = distribution.listProperties(DCAT.mediaType)
                .mapWith { it.`object`.visitWith(MediaTypeVisitor) as String }

            //mediaType.toList().forEach { log.info("${distribution.uri} has mediaType $it") }


            val formatIsCSV = format
                .filterKeep { it.getString("label", "").uppercase() == "CSV" }.hasNext() ||
                    mediaType
                        .filterKeep { (it ?: "") == "text/csv" }.hasNext()

            val accessURLs = distribution.listProperties(DCAT.accessURL)
                .filterKeep { it.`object`.isURIResource }
                .mapWith { it.asStatementUris() }.toList()
            val downloadURLs = distribution.listProperties(DCAT.downloadURL)
                .filterKeep { it.`object`.isURIResource }
                .mapWith { it.asStatementUris() }.toList()



            Distribution(accessURLs, downloadURLs, formatIsCSV)
        }
        .toList()
}


private fun Statement.asJsonObject() = jsonObjectOf(
    "distributionUri" to subject.uri,
    "property" to predicate.uri,
    "uri" to `object`.asResource().uri
)

private fun Statement.asStatementUris() = StatementUris(subject.uri, predicate.uri, `object`.asResource().uri)