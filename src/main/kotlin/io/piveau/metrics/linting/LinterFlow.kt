package io.piveau.metrics.linting

import io.piveau.metrics.Distribution
import io.piveau.pipe.PipeContext
import io.vertx.core.Future
import io.vertx.core.Vertx
import io.vertx.core.file.OpenOptions
import io.vertx.ext.web.client.WebClient
import io.vertx.ext.web.client.WebClientOptions
import io.vertx.ext.web.client.predicate.ResponsePredicate
import io.vertx.ext.web.codec.BodyCodec
import io.vertx.kotlin.coroutines.coAwait
import org.slf4j.LoggerFactory
import java.net.MalformedURLException
import java.net.URL
import java.util.concurrent.atomic.AtomicReference

data class Download(val distributionUri: String, val url: String, val path: String, val contentType: String)

class LinterFlow(private val vertx: Vertx) {
    private val log = LoggerFactory.getLogger(javaClass)
    private val client: WebClient = WebClient.create(
        vertx,
        WebClientOptions()
            .setMaxPoolSize(100)
            .setTrustAll(true)
            .setVerifyHost(false))

    suspend fun downloadDistribution(distribution: Distribution, pipeContext: PipeContext): Download? {
        for (url in distribution.downloadURLs) {
            try {
                return download(url.uri, url.distributionUri, true).coAwait()
            } catch (e: Exception) {
                pipeContext.log.error("Error while downloading $url", e)
            }
        }
        for (url in distribution.accessURLs) {
            try {
                return download(url.uri, url.distributionUri, true).coAwait()
            } catch (e: Exception) {
                pipeContext.log.error("Error while downloading $url", e)
            }
        }

        return null
    }

    private fun download(url: String, distributionUri: String, header: Boolean = false): Future<Download> {
        val downloadUrl = try {
            URL(url)
        } catch (e: MalformedURLException) {
            return Future.failedFuture(e)
        }

        val filePath = AtomicReference<String>()
        return vertx.fileSystem().createTempFile("piveau", ".tmp")
            .compose {
                filePath.set(it)
                vertx.fileSystem().open(it, OpenOptions().setWrite(true))
            }
            .compose { file ->
                val request = client.getAbs(downloadUrl.toString()).`as`(BodyCodec.pipe(file))
                    .expect(ResponsePredicate.SC_SUCCESS)
                if (header) {
                    request.putHeader("Accept", "text/csv")
                }
                request.timeout(10000).send()
            }
            .map { Download(distributionUri, url, filePath.get(), it.getHeader("Content-Type") ?: "") }
            .onFailure {
                cleanUp(filePath.get())
            }
    }

    private fun cleanUp(filePath: String) {
        vertx.fileSystem().exists(filePath)
            .onSuccess { exists ->
                if (exists) vertx.fileSystem().delete(filePath)
            }
    }

}