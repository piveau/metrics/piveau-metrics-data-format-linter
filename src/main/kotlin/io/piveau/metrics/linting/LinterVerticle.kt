package io.piveau.metrics.linting

import de.piveau.metrics.linter.csv.CSVLinter
import io.piveau.dqv.createMetricsGraph
import io.piveau.dqv.listMetricsModels
import io.piveau.metrics.Distribution
import io.piveau.metrics.datasetDistributions
import io.piveau.pipe.PipeContext
import io.piveau.rdf.*
import io.vertx.kotlin.coroutines.CoroutineEventBusSupport
import io.vertx.kotlin.coroutines.CoroutineVerticle
import org.apache.jena.query.DatasetFactory
import org.apache.jena.riot.Lang
import org.slf4j.LoggerFactory

class LinterVerticle : CoroutineVerticle(), CoroutineEventBusSupport {

    private lateinit var linterFlow: LinterFlow
    private val log = LoggerFactory.getLogger(javaClass)

    override suspend fun start() {
        linterFlow = LinterFlow(vertx)
        vertx.eventBus().coConsumer(ADDRESS) {
            handlePipe(it.body())
        }
    }

    private suspend fun handlePipe(context: PipeContext) {

        val maxErrors = config.getInteger("max_errors", 2000)
        val dataset = try {
            when {
                context.stringData.isNotEmpty() -> context.stringData.toByteArray()
                    .toDataset(RDFMimeTypes.TRIG.asRdfLang())

                context.binaryData.isNotEmpty() -> context.binaryData.toDataset(RDFMimeTypes.TRIG.asRdfLang())
                else -> DatasetFactory.create()
            }
        } catch (e: Exception) {
            context.log.error("Error while parsing dataset with dataInfo ${context.dataInfo.encode()}", e)
            log.error("Error while parsing dataset with dataInfo ${context.dataInfo.encode()}", e)
            context.pass()
            return
        }

        val distributions = dataset.defaultModel.datasetDistributions().filter(Distribution::formatIsCSV)

        if (distributions.isEmpty()) {
            if (context.log.isTraceEnabled) {
                context.log.trace("Dataset passed: ${context.dataInfo}")
            }
            context.pass()
        } else {
            val metricsModel = dataset.listMetricsModels().firstOrNull()
                ?: dataset.createMetricsGraph("urn:${context.pipe.header.context?.asNormalized()}:${context.pipe.header.name}")

            distributions.forEach { distribution ->
                linterFlow.downloadDistribution(distribution, context)?.let { download ->
                    try {
                        val result = CSVLinter(download.path, download.contentType, maxErrors + 50).validate()
                        metricsModel.add(result.toModel(download.distributionUri, maxErrors))
                        if(log.isDebugEnabled) {
                            log.debug("Validation result: ${download.distributionUri} from dataset ${context.dataInfo} ${if (result.isPassed) "passed" else "failed"} with ${result.count()} errors")
                        }
                    } catch (t: Throwable) {
                        context.log.error("Unexpected exception during validation: ${context.dataInfo}", t)
                    }
                }
            }
            context.log.info("Dataset validated: ${context.dataInfo}")
            context.setResult(dataset.presentAs(Lang.TRIG), RDFMimeTypes.TRIG, context.dataInfo).forward()
        }
    }

    companion object {
        const val ADDRESS: String = "io.piveau.pipe.metrics.linter"
    }

}
