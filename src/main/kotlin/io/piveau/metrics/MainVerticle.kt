package io.piveau.metrics

import io.piveau.metrics.linting.LinterVerticle
import io.piveau.pipe.connector.PipeConnector
import io.vertx.core.DeploymentOptions
import io.vertx.core.Launcher
import io.vertx.core.ThreadingModel
import io.vertx.kotlin.coroutines.CoroutineEventBusSupport
import io.vertx.kotlin.coroutines.CoroutineVerticle
import io.vertx.kotlin.coroutines.coAwait

class MainVerticle : CoroutineVerticle(), CoroutineEventBusSupport {

    override suspend fun start() {
        vertx.deployVerticle(LinterVerticle::class.java, DeploymentOptions().setThreadingModel(ThreadingModel.WORKER)).coAwait()
        PipeConnector.create(vertx, DeploymentOptions()).coAwait().publishTo(LinterVerticle.ADDRESS)
    }

}

fun main(args: Array<String>) {
    Launcher.executeCommand("run", *(args.plus(MainVerticle::class.java.name)))
}
